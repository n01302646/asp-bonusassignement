﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cartesian_Smartesian.aspx.cs" Inherits="BonusAssignment.Cartesian_Smartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background-color:#a1cca1">
    <form id="form1" runat="server">
        <div style ="margin:auto;width: 30%;border: 3px solid green; margin-top:15%; padding: 10px; background-color:#a0f7a0">
            <h3>Cartesian Smartesian</h3>
            <asp:Label runat="server" for ="xValue:">X Value</asp:Label>
            <asp:TextBox ID="xValue" runat="server"></asp:TextBox>
            <asp:CompareValidator runat="server" ErrorMessage="Enter  a non zero X-value" ControlToValidate="xValue" type="integer" ValuetoCompare="0" Operator="NotEqual"/>
            <br /><br /> <asp:Label runat="server" for ="yValue:">Y Value</asp:Label>
            <asp:TextBox ID="yValue" runat="server"></asp:TextBox>
            <asp:CompareValidator runat="server" ErrorMessage="Enter  a non zero Y-value" ControlToValidate="yValue" type="integer" ValuetoCompare="0" Operator="NotEqual"/>
            <br /><br /><asp:Button ID="Sumbit" runat="server" Text="Find Quadrant" OnClick="Sumbit_Click" />
        </div>
    </form>
      <br /><br > <div id="printOutput" style="text-align:center;" runat="server">     
    </div>
</body>
</html>
