﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="String_Bling.aspx.cs" Inherits="BonusAssignment.String_Bling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background-color:#a1cca1">
   <form id="form1" runat="server">
        <div style ="margin:auto;width: 30%;border: 3px solid green; margin-top:15%; padding: 10px; background-color:#a0f7a0">
             <h3>String_Bling</h3>
            <asp:Label runat="server" for ="InputStr">Enter String</asp:Label>
            <asp:TextBox id="InputStr" runat="server"></asp:TextBox>
           <asp:RegularExpressionValidator id="stringOnlyChecker" runat="server" ErrorMessage="Enter String only value" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="InputStr" />
 <br /><br /><asp:Button id="Sumbit" runat="server" Text="Check if Palindrome" onclick="Sumbit_Click" />
        </div>
        <br /><br /><div id="print_output" style="text-align: center;" runat="server"></div>
    </form>   
</body>
</html>
