﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class Cartesian_Smartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Sumbit_Click(object sender, EventArgs e)
        {
            int x = int.Parse(xValue.Text);
            int y = int.Parse(yValue.Text);
            if (x > 0 && y > 0) printOutput.InnerHtml = "Point Lies in 1st Quadrant";
            if (x < 0 && y > 0) printOutput.InnerHtml = "Point Lies in 2nd Quadrant";
            if (x < 0 && y < 0) printOutput.InnerHtml = "Point Lies in 3rd Quadrant";
            if (x > 0 && y < 0) printOutput.InnerHtml = "Point Lies in 4st Quadrant";
        }
    }
}