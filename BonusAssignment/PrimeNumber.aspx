﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrimeNumber.aspx.cs" Inherits="BonusAssignment.PrimeNumber" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background-color:#a1cca1">
    <form id="form1" runat="server">
        <div style ="margin:auto;width: 30%;border: 3px solid green; margin-top:15%; padding: 10px; background-color:#a0f7a0">
             <h3>Divisible Sizzable</h3>
            <asp:Label runat="server" for ="InputNo">Enter Number</asp:Label>
            <asp:TextBox id="InputNo" runat="server"></asp:TextBox>
            <asp:CompareValidator runat="server" ErrorMessage="Enter  a non zero number" ControlToValidate="InputNo" type="integer" ValuetoCompare="0" Operator="GreaterThan"/>
            <br /><br /><asp:Button id="Sumbit" runat="server" Text="Check if Prime" OnClick="Sumbit_Click" />
        </div>       
        <br /><br /><div id="print_output" style="text-align:center;" runat="server"> </div> 
     </form>
</body>
</html>
